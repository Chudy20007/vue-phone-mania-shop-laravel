import Vue from 'vue'
import Vuex from 'vuex'

import users from './modules/users'
import validation from './modules/validation'
import roles from './modules/roles'
import resolutions from './modules/resolutions'
import comments from './modules/comments'
import displays from './modules/displays'
import smartphones from './modules/smartphones'
import processors from './modules/processors'
import systems from './modules/systems'
import products from './modules/products'
import manufacturers from './modules/manufacturers'
import ram from './modules/ram'
import memories from './modules/memories'
import cameras from './modules/cameras'
import batteries from './modules/batteries'
import tags from './modules/tags'
import auth from './modules/auth'
import orders from './modules/orders'
import payments from './modules/payments'
import rates from './modules/rates'
import sorting from './modules/sorting'
import pagination from './modules/pagination'
import responseHandler from './modules/response_handler'
Vue.use(Vuex)

export default new Vuex.Store({
modules:{
    users,
    processors,
    products,
    comments,
    displays,
    smartphones,
    batteries,
    auth,
    tags,
    memories,
    roles,
    systems,
    ram,
    cameras,
    orders,
    resolutions,
    manufacturers,
    validation,
    sorting,
    payments,
    rates,
    pagination,
    responseHandler
    }
})