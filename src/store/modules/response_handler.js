const state = {
    error:false,
    success: false
}


const actions = {
   clearErrors({commit},value){
       commit('CLEARERROR',value);
   },
   clearResponse({commit},value){
    commit('CLEARRESPONSE',value);
}
}

const mutations = {
    PUSHERROR(state,response){
        if(typeof response.response != "undefined")
        state.error = response.response.data;
        else
        state.error = response.data;
    },
    CLEARERROR(state,value){
        state.error = value
    },
    CLEARRESPONSE(state,value){
        state.success = value
    },
    PUSHRESPONSE(state,response){
        if(typeof response.response != "undefined")
        state.success = response.response.data.message;
        else
        state.success = response.data.message;
    },
}

const getters = {
    error: state => {
        return state.error
      },
      success: state => {
        return state.success
      }
}


export default {
    state,
    actions,
    mutations,
    getters
}