const state = {
    resolutions: [],
    resolution: {},
    refresh: 0
  }
  
  const actions = {
    resolutions({
      commit
    }) {
      axios
        .get('resolutions',this.getters.header)
        .then(response => commit('RESOLUTIONS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    resolution({
      commit
    }, id) {
      axios
        .get(`resolutions/${id}`,this.getters.header)
        .then(response => commit('RESOLUTION', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateResolution({
      commit
    }, resolution) {
      axios.put(`resolutions/${resolution.id}`, resolution,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteResolution({
      commit
    }, id) {
      axios.delete(`resolutions/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeResolution({
      commit
    }, resolution) {
      axios.post(`/resolutions`, resolution,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    resolutions: state => {
      return state.resolutions;
    },
  
    resolution: state => {
      return state.resolution;
    },
  
  }
  
  
  const mutations = {
    RESOLUTIONS: (state, resolutions) =>
      state.resolutions = resolutions,
      RESOLUTION: (state, resolution) =>
      state.resolution = resolution,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  