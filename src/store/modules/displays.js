const state = {
    displays: [],
    display: {},
    refresh: 0
  }
  
  const actions = {
    displays({
      commit
    }) {
      axios
        .get('displays',this.getters.header)
        .then(response => commit('DISPLAYS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    display({
      commit
    }, id) {
      axios
        .get(`displays/${id}`,this.getters.header)
        .then(response => commit('DISPLAY', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateDisplay({
      commit
    }, display) {
      axios.put(`displays/${display.id}`, display,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteDisplay({
      commit
    }, id) {
      axios.delete(`displays/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeDisplay({
      commit
    }, display) {
      axios.post(`/displays`, display,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    displays: state => {
      return state.displays;
    },
  
    display: state => {
      return state.display;
    },
  
  }
  
  
  const mutations = {
    DISPLAYS: (state, displays) =>
      state.displays = displays,
    DISPLAY: (state, display) =>
    state.display = display,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  