import { mapGetters, mapActions } from "vuex";

 

const state = {
    smartphones: [],
    smartphone: {},
    ready: 0,
    results: 0,
    deleted:0
  }
  
  const actions = {
    getDetails({commit}){
      axios.get('smartphones/create',this.getters.header)
      .then(response=> {
        commit('DISPLAYS',response.data.data.displays);
        commit('BATTERIES',response.data.data.batteries);
        commit('RAMS',response.data.data.rams);
        commit('MEMORIES',response.data.data.memories);
        commit('RESOLUTIONS',response.data.data.resolutions);
        commit('PROCESSORS',response.data.data.processors);
        commit('CAMERAS',response.data.data.cameras);
        commit('MANUFACTURERS',response.data.data.manufacturers);
        commit('SYSTEMS',response.data.data.systems);
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
    },
    getDetailsForUpdateSmartphone({commit},id){
      axios.get(`smartphones/${id}/edit`,this.getters.header)
      .then(response=> {
        commit('SMARTPHONE',response.data.data.smartphone);
        commit('DISPLAYS',response.data.data.displays);
        commit('BATTERIES',response.data.data.batteries);
        commit('RAMS',response.data.data.rams);
        commit('MEMORIES',response.data.data.memories);
        commit('RESOLUTIONS',response.data.data.resolutions);
        commit('PROCESSORS',response.data.data.processors);
        commit('CAMERAS',response.data.data.cameras);
        commit('MANUFACTURERS',response.data.data.manufacturers);
        commit('SYSTEMS',response.data.data.systems);
        commit('READY', state.ready++)
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });

    },
    smartphones({
      commit
    }) {
      axios
        .get('smartphones')
        .then(response => {
        commit('SMARTPHONES', response.data.data)
        commit('READY')})
        .catch(error => {
          commit('PUSHERROR', error);
     
        });
    },
    smartphone({
      commit
    }, id) {
      axios.get(`smartphones/${id}`,this.getters.header)
      .then(response=> {
        commit('SMARTPHONE',response.data.data);
        commit('READY')
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
    },
  
    updateSmartphone({
      commit
    }, data) {
      axios.put(`smartphones/${data.id}`, data,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteSmartphone({
      commit
    }, id) {
      axios.delete(`smartphones/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('DELETED');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeSmartphone({
      commit
    }, data) {
      axios.post(`/smartphones`, data,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    },
    findProducts({commit},productsToFind) {
      axios.get(`/find/${productsToFind}`)
      .then((response) => {
        commit('SMARTPHONES', response.data.data);
        commit('RESULTS') 
      })
      .catch(error => {
        commit('PUSHERROR', error);  
    })
  }
  
  }
  
  const getters = {
    smartphones: state => {
      return state.smartphones;
    },
  
    smartphone: state => {
      return state.smartphone;
    },
    ready: state => {
      return state.ready
    },
    results: state => {
      return state.results
    },
    deleted : state => {
      return state.deleted
    }

  
  }
  
  
  const mutations = {
    GETDETAILS(state,details){
      //console.log(this.state.displays);
    //  this.state.processors = details.processors;
    //  this.state.rams = details.rams;
    //  this.state.memories - details.memories;
        commit('DISPLAYS',details.displays);
   //   this.state.resolutions = details.resolutions;
  //    this.state.cameras = details.cameras;
   //   this.state.batteries = details.batteries;
  //    this.state.manufacturers = details.manufacturers;
    
    },

    SMARTPHONES: (state, smartphones) =>
      state.smartphones = smartphones,
    SMARTPHONE: (state, smartphone) =>
      state.smartphone = smartphone,
    REFRESH: state =>
      state.refresh = ++state.refresh,
    DELETED: state =>
      state.deleted = ++state.deleted,
    RESULTS: state =>
      state.results = ++state.results,
    READY: state =>
      state.ready = ++state.ready,
    RESETREADY: state =>
      state.ready = 0,
    RESETRESULTS: state =>
      state.results = 0
     }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  