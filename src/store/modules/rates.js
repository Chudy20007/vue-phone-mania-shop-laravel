const state = {
    rates: [],
    rate: {},
    refresh: 0
  }
  
  const actions = {
    rates({
      commit
    }) {
      axios
        .get('rates',this.getters.header)
        .then(response => commit('RATES', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    rate({
      commit
    }, id) {
      axios
        .get(`rates/${id}`,this.getters.header)
        .then(response => commit('RATE', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateRate({
      commit
    }, rate) {
      axios.put(`rates/${rate.id}`, rate,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteRate({
      commit
    }, id) {
      axios.delete(`rates/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeRate({
      commit
    }, rate) {
      axios.post(`/rates`, rate,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');  
        }).catch(error => {
          commit('REFRESH'); 
          commit('PUSHERROR', error)
        });
    }
  }
  
  const getters = {
    rates: state => {
      return state.rates;
    },
  
    rate: state => {
      return state.rate;
    },
  
  }
  
  
  const mutations = {
    RATES: (state, rates) =>
      state.rates = rates,
    RATE: (state, rate) =>
      state.rate = rate,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  