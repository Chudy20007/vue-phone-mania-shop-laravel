const state = {
  tags: [],
  tag: {},
  refresh: 0
}

const actions = {
  tags({
    commit
  }) {
    axios
      .get('tags',this.getters.header)
      .then(response => commit('TAGS', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  tag({
    commit
  }, id) {
    axios
      .get(`tags/${id}`,this.getters.header)
      .then(response => commit('TAG', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  updateTag({
    commit
  }, tag) {
    axios.put(`tags/${tag.id}`, tag,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  deleteTag({
    commit
  }, id) {
    axios.delete(`tags/${id}`,this.getters.header)
    .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  storeTag({
    commit
  }, tag) {
    axios.post(`/tags`, tag,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');


      }).catch(error => {
        commit('PUSHERROR', error)
      });
  }

}

const getters = {
  tags: state => {
    return state.tags;
  },

  tag: state => {
    return state.tag;
  },

}


const mutations = {
  TAGS: (state, tags) =>
    state.tags = tags,
  TAG: (state, tag) =>
    state.tag = tag,
  REFRESH: state =>
    state.refresh = ++state.refresh
}

export default {
  state,
  actions,
  mutations,
  getters
}
