const state = {
    payments: [],
    payment: {},
    refresh: 0
  }
  
  const actions = {
    payments({
      commit
    }) {
      axios
        .get('payments',this.getters.header)
        .then(response => commit('PAYMENTS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    payment({
      commit
    }, id) {
      axios
        .get(`payments/${id}`,this.getters.header)
        .then(response => commit('PAYMENT', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updatePayment({
      commit
    }, payment) {
      axios.put(`payments/${payment.id}`, payment,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deletePayment({
      commit
    }, id) {
      axios.delete(`payments/${id}`,this.getters.header)
      .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storePayment({
      commit
    }, payment) {
      axios.post(`/payments`, payment,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    payments: state => {
      return state.payments;
    },
  
    payment: state => {
      return state.payment;
    }
  }
  
  
  const mutations = {
      PAYMENTS: (state, payments) =>
          state.payments = payments,
      PAYMENT: (state, payment) =>
          state.payment = payment,
      REFRESH: state =>
          state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  