const state = {
    batteries: [],
    battery: {},
    refresh: 0
  }
  
  const actions = {
    batteries({
      commit
    }) {
      axios
        .get('batteries',this.getters.header)
        .then(response => commit('BATTERIES', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    battery({
      commit
    }, id) {
      axios
        .get(`batteries/${id}`,this.getters.header)
        .then(response => commit('BATTERY', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateBattery({
      commit
    }, battery) {
      axios.put(`batteries/${battery.id}`, battery,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteBattery({
      commit
    }, id) {
      axios.delete(`batteries/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeBattery({
      commit
    }, battery) {
      axios.post(`/batteries`, battery,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    batteries: state => {
      return state.batteries;
    },
  
    battery: state => {
      return state.battery;
    },
  
  }
  
  
  const mutations = {
    BATTERIES: (state, batteries) =>
      state.batteries = batteries,
    BATTERY: (state, battery) =>
      state.battery = battery,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  