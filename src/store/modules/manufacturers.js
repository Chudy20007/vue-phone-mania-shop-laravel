const state = {
  manufacturers: [],
  manufacturer: {},
  refresh: 0
}

const actions = {
  searchProductsByManufacturers({commit},manufacturers){
    var man = { manufacturers};
    axios.post('find-products-by-manufacturers',manufacturers)
    .then(response => {
      commit('SMARTPHONES', response.data.data)
    })
    .catch(error => {
      commit('PUSHERROR', error)
    });
  },
  manufacturers({
    commit
  }) {
    axios
      .get('manufacturers',this.getters.header)
      .then(response => commit('MANUFACTURERS', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  manufacturer({
    commit
  }, id) {
    axios
      .get(`manufacturers/${id}`,this.getters.header)
      .then(response => commit('MANUFACTURER', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  updateManufacturer({
    commit
  }, manufacturer) {
    axios.put(`manufacturers/${manufacturer.id}`, manufacturer,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  deleteManufacturer({
    commit
  }, id) {
    axios.delete(`manufacturers/${id}`,this.getters.header)
    .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  storeManufacturer({
    commit
  }, manufacturer) {
    axios.post(`/manufacturers`, manufacturer,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
      }).catch(error => {
        commit('PUSHERROR', error)
      });
  }

}

const getters = {
  manufacturers: state => {
    return state.manufacturers;
  },

  manufacturer: state => {
    return state.manufacturer;
  },

  refresh: state => {
    return state.refresh;
  }
}


const mutations = {
    MANUFACTURERS: (state, manufacturers) =>
        state.manufacturers = manufacturers,
    MANUFACTURER: (state, manufacturer) =>
        state.manufacturer = manufacturer,
    REFRESH: state =>
        state.refresh = ++state.refresh
}

export default {
  state,
  actions,
  mutations,
  getters
}
