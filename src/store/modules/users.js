const state = {
  users: [],
  user: {},
  basket:[],
  productCount:0,
  count:0,
  response: 0
}

const actions = {

  users() {
    axios
      .get('users',this.getters.header)
      .then(response => (state.users = response.data.data))
  },
addToBasket({commit},product)
{
  commit('ADDTOBASKET', product);
  commit('PRODUCTCOUNT',product.count);
  commit('REFRESH');
},
verifyUser({commit},token){
  axios.get(`user/verify/${token}`).then((response) => {
    commit('PUSHRESPONSE', response);
    commit('REFRESH');
  }).catch(error => {
    commit('PUSHERROR', error)
  });
},

sendForgotPasswordRequest({commit},email){
  axios.post('/email',{email:email}).then((response) => {
    commit('PUSHRESPONSE', response);
    commit('TOKEN', response.token);
    commit('REFRESH');
  }).catch(error => {
    commit('PUSHERROR', error)
  });
},
sendResetPasswordRequest({commit},user){
  axios.post('/reset',
  {
    email:user.email,
    password:user.password,
    password_confirmation:user.confirm_password,
    token:user.token
  }).then((response) => {
    commit('PUSHRESPONSE', response);
    commit('REFRESH');
  }).catch(error => {
    commit('PUSHERROR', error)
  });
},
  user({
    commit
  }, id) {
    axios.get(`users/${id}`,this.getters.header).then(response => commit('USER', response.data.data));
  },
  /*
  sortUsersList ({commit},selectedValue) { 
    return state.users.sort((a, b)=>{
        var x = a[selectedValue].toLowerCase();
      var y = b[selectedValue].toLowerCase();
      if (x < y) {return -1;}
      if (x > y) {return 1;}
      return 0;
  
  });
},
prepareOrderIcons({commit}, count){
  commit('ORDERICONSCREATE',count);
}, */
  deleteUser({
    commit
  }, id) {
    axios.delete(`users/${id}`,this.getters.header)
      .then(() => commit('REFRESH'))
  },
  storeUser({
    commit
  }, user) {
    axios.post(`/users`,user)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      }).catch(error => {
        commit('PUSHERROR', error)
      });
  },
/*  login({
    commit
  }, user) {
    axios.post(`/login`, user)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');


      }).catch(error => {
        commit('PUSHERROR', error)
      });
  },
  */
  updateUser({
    commit
  }, user) {
    axios.put(`/users/${user.id}`, user,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      }).catch(error => {
        commit('PUSHERROR', error)
      });
  },
  


}

const mutations = {
  USERS(state, users) {
    state.users = users;
  },
  USER(state, user) {
    state.user = user;
  },
  TOKEN(state, token) {
    state.user.token = token;
  },
  USERROLE(state,role){

   state.user.role =role;
   state.user.password='';
  },
  REFRESH: state =>
    state.refresh = ++state.refresh,
  PRODUCTCOUNT(state, c = 1){
    state.productCount = Number(state.productCount) + Number(c);
  },
    ADDTOBASKET(state,product) {
      if (state.basket.some(e => e.id === product.id,product)) {
        var elementInBasket = state.basket.find(function(element) {
          return element.id ==  product.id;
        });

        if(product.count > 1)
        elementInBasket.count = Number(elementInBasket.count) + Number(product.count);
        else
        elementInBasket.count = Number(elementInBasket.count) + Number(1);

      } else
        state.basket.push(product);

    }

}

/*
  ORDER: (state,columnIndex) => {
  state.order *= -1;
  mutations.ORDERICONSRESET(state);
  state.order_icons[columnIndex] = true;
  },
  ORDERICONSCREATE(state,count){
    for(var i=0; i < count; i++)   
    state.order_icons[i] = false;
  },
  ORDERICONSRESET(state){
    state.order_icons.forEach(function(val,index) {
     state.order_icons[index] = false;  
  }); 

},
*/


const getters = {
  users: state => {
    return state.users;
  },
  user: state => {
    return state.user;
  },
  basket: state => {
    return state.basket;
  },
  productCount: state => {
    return state.productCount
  },  
}

/* order: state => {
    return state.order
  },
  orderIcons: state => {
    return state.order_icons
  }
*/


export default {
  state,
  actions,
  mutations,
  getters
}
