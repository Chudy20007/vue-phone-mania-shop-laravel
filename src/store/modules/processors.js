const state = {
    processors: [],
    processor: {},
    refresh: 0
  }
  
  const actions = {
    processors({
      commit
    }) {
      axios
        .get('processors',this.getters.header)
        .then(response => commit('PROCESSORS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    processor({
      commit
    }, id) {
      axios
        .get(`processors/${id}`,this.getters.header)
        .then(response => commit('PROCESSOR', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateProcessor({
      commit
    }, processor) {
      axios.put(`processors/${processor.id}`, processor,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteProcessor({
      commit
    }, id) {
      axios.delete(`processors/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeProcessor({
      commit
    }, processor) {
      axios.post(`/processors`, processor,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    processors: state => {
      return state.processors;
    },
  
    processor: state => {
      return state.processor;
    },
  
  }
  
  
  const mutations = {
    PROCESSORS: (state, processors) =>
      state.processors = processors,
    PROCESSOR: (state, processor) =>
      state.processor = processor,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  