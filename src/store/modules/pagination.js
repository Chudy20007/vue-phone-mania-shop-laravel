
  const actions = {
pageCount({commit},[list, size]){
  console.log(list);
    let l = list.length,
        s = size;
    return Math.floor(l/s);
  },
  paginatedData({commit},[list,pageNumber, size]){
    const start = pageNumber * size,
          end = start + size;
          console.log(list);
    return list.slice(start, end);
  },
  nextPage({commit},pageNumber){
    return pageNumber++;
 },
 prevPage({commit},pageNumber){
  return  pageNumber--;
 }
}
export default {
  actions
}