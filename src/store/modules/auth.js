import publicKey from './../../jwt/public'
import jwt from 'jsonwebtoken'
const state = {
    logged:false,
    header:'',
    redirect:''
}

const getters = {
    logged:state => {
        return state.logged
    },
    redirect:state => {
        return state.redirect
    },
    header:state => {
        return state.header
    },
    token: state => {
        return state.token
    }

}
const actions = {
    login({commit},loginData){
        axios.post('login',loginData)
        .then(result => {
            try {
           jwt.verify(result.data.access_token,publicKey, function(error, decoded){
               if(!error){
                var config = {
                    headers: {'Authorization': " Bearer " + result.data.access_token }
                };
                   commit('HEADER',config);
                   commit('LOGGED',true);
                   commit('USERROLE',result.data.role.original);
                   commit('PUSHRESPONSE', result);
                   commit ('REFRESH');
                   localStorage.setItem('token',result.data.access_token)
                   localStorage.setItem('role',result.data.role.original)
                  // commit('TOKEN',result.data.access_token);
               }
               else {
                commit('PUSHERROR', error)
               }
           }) }
           catch(err) {
            commit('PUSHERROR', error);
           }
        }).catch(error => {
            commit('PUSHERROR', error)
        })
    },
    logout({commit}){
        axios.post('logout',null,this.getters.header)
        .then(result => {
            commit('LOGGED',false);
            commit('HEADER','');
            commit('PUSHRESPONSE', result);
            commit('USERROLE','');
            localStorage.removeItem('token');
            localStorage.removeItem('role');
        })
    },
    refreshToken({commit}){
        axios.get('authenticate/refresh',this.getters.header)
        .then(result =>{
                 var config = {
                     headers: {'Authorization': " Bearer " + result.data.access_token }
                 };
                 commit('HEADER',config);
                 commit('PUSHRESPONSE', result);
        })
    }
}

const mutations = {
    LOGGED(state,status){
        state.logged = status;
    },
    HEADER(state,header){
        state.header = header;
    },
    TOKEN(state, token){
        state.token = token;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}