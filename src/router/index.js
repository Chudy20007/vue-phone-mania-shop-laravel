import Vue from 'vue'
import Router from 'vue-router'
import App from '../App'
import ManufacturersList from '@/components/manufacturers/List'
import UsersList from '@/components/users/List'
import RolesList from '@/components/roles/List'
import TagsList from '@/components/tags/List'
import ResolutionsList from '@/components/resolutions/List'
import CamerasList from '@/components/cameras/List'
import RamList from '@/components/ram_memories/List'
import DisplaysList from '@/components/displays/List'
import CommentsList from '@/components/comments/List'
import BatteriesList from '@/components/batteries/List'
import MemoriesList from '@/components/memories/List'
import SmartphonesList from '@/components/smartphones/List'
import ProcessorsList from '@/components/processors/List'
import OperatingSystemsList from '@/components/systems/List'
import PaymentsList from '@/components/payments/List'
import OrdersList from '@/components/orders/List'
import UserOrdersList from '@/components/users/UserOrdersList'
import EditManufacturer from '@/components/manufacturers/Edit'
import EditTag from '@/components/tags/Edit'
import EditUser from '@/components/users/Edit'
import EditRole from '@/components/roles/Edit'
import EditResolution from '@/components/resolutions/Edit'
import EditCamera from '@/components/cameras/Edit'
import EditRam from '@/components/ram_memories/Edit'
import EditDisplay from '@/components/displays/Edit'
import EditBattery from '@/components/batteries/Edit'
import EditPayment from '@/components/payments/Edit'
import EditMemory from '@/components/memories/Edit'
import EditSmartphone from '@/components/smartphones/Edit'
import EditProcessor from '@/components/processors/Edit'
import EditComment from '@/components/comments/Edit'
import EditOperatingSystem from '@/components/systems/Edit'
import EditOrder from '@/components/orders/Edit'
import EditOrderStatus from '@/components/orders/EditOrderStatus'
import AddManufacturer from '@/components/manufacturers/Add'
import AddTag from '@/components/tags/Add'
import AddRole from '@/components/roles/Add'
import AddUser from '@/components/users/Add'
import AddComment from '@/components/comments/Add'
import AddPayment from '@/components/payments/Add'
import AddResolution from '@/components/resolutions/Add'
import AddCamera from '@/components/cameras/Add'
import AddRam from '@/components/ram_memories/Add'
import AddDisplay from '@/components/displays/Add'
import AddBattery from '@/components/batteries/Add'
import AddMemory from '@/components/memories/Add'
import AddSmartphone from '@/components/smartphones/Add'
import AddProcessor from '@/components/processors/Add'
import AddOperatingSystem from '@/components/systems/Add'
import ProductsList from '@/components/smartphones/SmartphonesList'
import ShowSmartphone from '@/components/smartphones/Smartphone'
import Basket from '@/components/users/Basket'
import Register from '@/components/users/Register'
import Login from '@/components/users/Login'
import VerifyUser from '@/components/users/Verify'
import ForgotPassword from '@/components/users/ForgotPassword'
import ResetPassword from '@/components/users/ResetPassword'
import Checkout from '@/components/orders/Checkout'
import Rules from '@/components/users/Rules'
import AccessDenied from '@/components/auth/Denied'
import jwt from 'jsonwebtoken'
import publicKey from '@/jwt/public'
import store from '../store/index';

Vue.use(Router)
const routes = [{
    path: '/reset/:token',
    name: 'resetPassword',
    component: ResetPassword
  },
  {
    path: '/denied',
    name: 'denied',
    component: AccessDenied
  },
  {
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('token') != null) {
        localStorage.removeItem('token')
        localStorage.removeItem('role')
        store.commit('LOGGED');
      }
      return next({
        name: 'products'
      })
    },
    path: '/logout',
    name: 'logout',
  },
  {
    path: '/rules',
    name: 'rules',
    component: Rules,
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: Checkout,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/user/verify/:token',
    name: 'verifyUser',
    component: VerifyUser
  },
  {
    path: '/reset',
    name: 'forgot-password',
    component: ForgotPassword
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/payments',
    name: 'payments',
    component: PaymentsList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/user-orders',
    name: 'user-orders',
    component: UserOrdersList,
    meta: {
      requiresAuth: true,
      roles: ['administrator','user', 'worker']
    }
  },
  {
    path: '/order-status/:id/edit',
    name: 'order-status',
    component: EditOrderStatus,
    meta: {
      requiresAuth: true,
      roles: ['administrator', 'worker']
    }
  },
  {
    path: '/users',
    name: 'users',
    component: UsersList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/authenticate/refresh',
    name: 'refreshToken',
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/basket',
    name: 'basket',
    component: Basket,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/products',
    name: 'products',
    component: ProductsList,
  },
  {
    path: '/',
    name: 'Home',
    component: App
  },
  {
    path: '/smartphones',
    name: 'smartphones',
    component: SmartphonesList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/comments',
    name: 'comments',
    component: CommentsList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/systems',
    name: 'systems',
    component: OperatingSystemsList,
    meta: {
      requiresAuth: true,
     roles:['administrator'] 
    }
  },
  {
    path: '/cameras',
    name: 'cameras',
    component: CamerasList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/tags',
    name: 'tags',
    component: TagsList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/memories',
    name: 'memories',
    component: MemoriesList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/batteries',
    name: 'batteries',
    component: BatteriesList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/displays',
    name: 'displays',
    component: DisplaysList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/manufacturers',
    name: 'manufacturers',
    component: ManufacturersList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/roles',
    name: 'roles',
    component: RolesList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/processors',
    name: 'processors',
    component: ProcessorsList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/rams',
    name: 'rams',
    component: RamList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/orders',
    name: 'orders',
    component: OrdersList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/resolutions',
    name: 'resolutions',
    component: ResolutionsList,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/batteries/:id/edit',
    name: 'EditBattery',
    component: EditBattery,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/systems/:id/edit',
    name: 'EditSystem',
    component: EditOperatingSystem,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/comments/:id/edit',
    name: 'EditComment',
    component: EditComment,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/payments/:id/edit',
    name: 'EditPayment',
    component: EditPayment,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/tags/:id/edit',
    name: 'EditTag',
    component: EditTag,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/smartphones/:id/edit',
    name: 'EditSmartphone',
    component: EditSmartphone,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/memories/:id/edit',
    name: 'EditMemory',
    component: EditMemory,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/processors/:id/edit',
    name: 'EditProcessor',
    component: EditProcessor,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/cameras/:id/edit',
    name: 'EditCamera',
    component: EditCamera,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/displays/:id/edit',
    name: 'EditDisplay',
    component: EditDisplay,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/manufacturers/:id/edit',
    name: 'EditManufacturer',
    component: EditManufacturer,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/users/:id/edit',
    name: 'EditUser',
    component: EditUser,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/roles/:id/edit',
    name: 'EditRole',
    component: EditRole,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/resolutions/:id/edit',
    name: 'EditResolution',
    component: EditResolution,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/rams/:id/edit',
    name: 'EditRam',
    component: EditRam,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/orders/:id/edit',
    name: 'EditOrder',
    component: EditOrder,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/manufacturers/create',
    name: 'ManufacturerCreate',
    component: AddManufacturer,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/systems/create',
    name: 'SystemCreate',
    component: AddOperatingSystem,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/comments/create',
    name: 'Commentreate',
    component: AddComment,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/batteries/create',
    name: 'BatteryCreate',
    component: AddBattery,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/payments/create',
    name: 'PaymentCreate',
    component: AddPayment,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/processors/create',
    name: 'ProcessorCreate',
    component: AddProcessor,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/smartphones/create',
    name: 'SmartphoneCreate',
    component: AddSmartphone,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/cameras/create',
    name: 'CameraCreate',
    component: AddCamera,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/displays/create',
    name: 'DisplayCreate',
    component: AddDisplay,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/memories/create',
    name: 'MemoryCreate',
    component: AddMemory,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/tags/create',
    name: 'TagCreate',
    component: AddTag,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/roles/create',
    name: 'RoleCreate',
    component: AddRole,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/users/create',
    name: 'UserCreate',
    component: AddUser,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/resolutions/create',
    name: 'ResolutionCreate',
    component: AddResolution,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/rams/create',
    name: 'RamCreate',
    component: AddRam,
    meta: {
      requiresAuth: true,
      roles: ['administrator']
    }
  },
  {
    path: '/smartphones/:id',
    name: 'ShowSmartphone',
    component: ShowSmartphone
  }
]

const router = new Router({routes,mode:'history'})
export default router;
router.beforeEach((to, from, next) => {
  let authUser = {
    role: localStorage.getItem('role'),
    token: localStorage.getItem('token')
  }

  if (authUser && authUser.token && to.meta.requiresAuth) {
    jwt.verify(authUser.token,publicKey,err => {
      if(err){
        console.log("ERROR");
       return next({
          name: 'logout'
        }) 
      }
  });
  }

  if (!to.meta.requiresAuth)
    return next();

  if (!authUser || !authUser.token) {
    return next({
      name: 'login'
    })
  }


  if (!to.meta.roles || to.meta.roles.includes(authUser.role)) {
    return next()
  }
  next({
    name: 'denied'
  })
})

