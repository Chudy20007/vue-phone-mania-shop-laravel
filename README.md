# Vue Phone-Mania-Laravel

## Introduction

Vue Phone-Mania-Laravel is an frontend SPA (Single-page application) application based on the framework Vue.js. 
This APP allows you to connect with Vue Phone Mania Shop API (written in Laravel: [API](https://gitlab.com/Chudy20007/phone-mania-api-laravel)) and manage your shop/wiki.

## Features

*  CRUD for all controllers in the default API ([Phone Mania API - Laravel](https://gitlab.com/Chudy20007/phone-mania-api-laravel))
*  Full authorization with authentication (validators, policies)
*  Authorization type:  Authorization Bearer (public and private key)
*  Data validation before send
*  Animations
*  Friendly and simply GUI
*  Application logic divided into modules

## Used Components

* Animations
* Template Syntax
* Vue instance (with Vuex and Router)
* Data binding
* Events
* Routing
* Axios
* JWT Token (from the API)


## Installation

1.  Clone the project: `git clone https://github.com/Chudy20007/vue-phone-mania-shop-laravel`.
2.  In the CLI (npm) pass this command: `npm install ` (remember: run this command when you are in the default project folder). 
3.  Configure **.index** file (details in the next subsection).
4.  Run application: `npm run dev` (you can specify ip address and port).

## Configuration

*You must specify environment variables in the **.env** file.*

Example:
1. API_URL=http://phonemania.pl/api - specify  API URL.
2. In the **index.js** (folder called config) you can specify various dev server settings).
3. In the folder called **jwt** to the file **public.js** you must copy generated public key (example in the env.js file) from the API - [Phone Mania API - Laravel](https://gitlab.com/Chudy20007/phone-mania-api-laravel).

**Definition the public key in the env file:**

```
const publicKey = `
-----BEGIN PUBLIC KEY-----
YOUR PUBLIC KEY
-----END PUBLIC KEY-----
`
export default publicKey
```


*If you have a problem with token - check Phone Mania API Docs -> [Phone Mania API](https://gitlab.com/Chudy20007/phone-mania-api-laravel).*

## Available URL
<table style="width: 100%;">
    <tr>
        <td width="30%"><b>Name</b></td>
        <td width="70%"><b>Path</b></td>
    </tr>
    <tr>
        <td>resetPassword</td>
        <td>/reset/:token</td>
    </tr>
    <tr>
        <td>denied</td>
        <td>/denied</td>
    </tr>
    <tr>
        <td>logout</td>
        <td>/logout</td>
    </tr>
    <tr>
        <td>rules</td>
        <td>/rules</td>
    </tr>
    <tr>
        <td>checkout</td>
        <td>/checkout</td>
    </tr>
    <tr>
        <td>verifyUser</td>
        <td>/user/verify/:token</td>
    </tr>
    <tr>
        <td>forgot-password</td>
        <td>/reset</td>
    </tr>
    <tr>
        <td>register</td>
        <td>/register</td>
    </tr>
    <tr>
        <td>login</td>
        <td>/login</td>
    </tr>
    <tr>
        <td>payments</td>
        <td>/payments</td>
    </tr>
    <tr>
        <td>user-orders</td>
        <td>/user-orders</td>
    </tr>
    <tr>
        <td>order-status</td>
        <td>/order-status/:id/edit</td>
    </tr>
    <tr>
        <td>users</td>
        <td>/users</td>
    </tr>
    <tr>
        <td>refreshToken</td>
        <td>/authenticate/refresh</td>
    </tr>
    <tr>
        <td>basket</td>
        <td>/basket</td>
    </tr>
    <tr>
        <td>products</td>
        <td>/products</td>
    </tr>
    <tr>
        <td>Home</td>
        <td>/</td>
    </tr>
    <tr>
        <td>smartphones</td>
        <td>/smartphones</td>
    </tr>
    <tr>
        <td>systems</td>
        <td>/systems</td>
    </tr>
    <tr>
        <td>cameras</td>
        <td>/cameras</td>
    </tr>
    <tr>
        <td>tags</td>
        <td>/tags</td>
    </tr>
    <tr>
        <td>memories</td>
        <td>/memories</td>
    </tr>
    <tr>
        <td>batteries</td>
        <td>/batteries</td>
    </tr>
    <tr>
        <td>displays</td>
        <td>/displays</td>
    </tr>
    <tr>
        <td>manufacturers</td>
        <td>/manufacturers</td>
    </tr>
    <tr>
        <td>roles</td>
        <td>/roles</td>
    </tr>
    <tr>
        <td>processors</td>
        <td>/processors</td>
    </tr>
    <tr>
        <td>rams</td>
        <td>/rams</td>
    </tr>
    <tr>
        <td>orders</td>
        <td>/orders</td>
    </tr>
    <tr>
        <td>resolutions</td>
        <td>/resolutions</td>
    </tr>
    <tr>
        <td>EditBattery</td>
        <td>/batteries/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteBattery</td>
        <td>/batteries/:id</td>
    </tr>
    <tr>
        <td>EditSystem</td>
        <td>/systems/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteSystem</td>
        <td>/systems/:id</td>
    </tr>
    <tr>
        <td>EditPayment</td>
        <td>/payments/:id/edit</td>
    </tr>
        <tr>
        <td>DeletePayment</td>
        <td>/payments/:id</td>
    </tr>
    <tr>
        <td>EditTag</td>
        <td>/tags/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteTag</td>
        <td>/tags/:id</td>
    </tr>
    <tr>
        <td>EditSmartphone</td>
        <td>/smartphones/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteSmartphone</td>
        <td>/smartphones/:id</td>
    </tr>
    <tr>
        <td>EditMemory</td>
        <td>/memories/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteMemory</td>
        <td>/memories/:id</td>
    </tr>
    <tr>
        <td>EditProcessor</td>
        <td>/processors/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteProcessor</td>
        <td>/processors/:id</td>
    </tr>
    <tr>
        <td>EditCamera</td>
        <td>/cameras/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteCamera</td>
        <td>/cameras/:id</td>
    </tr>
    <tr>
        <td>EditDisplay</td>
        <td>/displays/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteDisplays</td>
        <td>/displays/:id</td>
    </tr>
    <tr>
        <td>EditManufacturer</td>
        <td>/manufacturers/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteManufacturer</td>
        <td>/manufacturers/:id</td>
    </tr>
    <tr>
        <td>EditUser</td>
        <td>/users/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteUser</td>
        <td>/users/:id</td>
    </tr>
    <tr>
        <td>EditRole</td>
        <td>/roles/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteRole</td>
        <td>/roles/:id</td>
    </tr>
    <tr>
        <td>EditResolution</td>
        <td>/resolutions/:id/edit</td>
    </tr>
    <tr>
        <td>DeleteResolution</td>
        <td>/resolutions/:id</td>
    </tr>
    <tr>
        <td>EditRam</td>
        <td>/rams/:id/edit</td>
    </tr>
    <tr>
        <td>DeleteRam</td>
        <td>/rams/:id</td>
    </tr>
    <tr>
        <td>EditOrder</td>
        <td>/orders/:id/edit</td>
    </tr>
        <tr>
        <td>DeleteOrder</td>
        <td>/orders/:id</td>
    </tr>
    <tr>
        <td>ManufacturerCreate</td>
        <td>/manufacturers/create</td>
    </tr>
    <tr>
        <td>SystemCreate</td>
        <td>/systems/create</td>
    </tr>
    <tr>
        <td>BatteryCreate</td>
        <td>/batteries/create</td>
    </tr>
    <tr>
        <td>PaymentCreate</td>
        <td>/payments/create</td>
    </tr>
    <tr>
        <td>ProcessorCreate</td>
        <td>/processors/create</td>
    </tr>
    <tr>
        <td>SmartphoneCreate</td>
        <td>/smartphones/create</td>
    </tr>
    <tr>
        <td>CameraCreate</td>
        <td>/cameras/create</td>
    </tr>
    <tr>
        <td>DisplayCreate</td>
        <td>/displays/create</td>
    </tr>
    <tr>
        <td>MemoryCreate</td>
        <td>/memories/create</td>
    </tr>
    <tr>
        <td>TagCreate</td>
        <td>/tags/create</td>
    </tr>
    <tr>
        <td>RoleCreate</td>
        <td>/roles/create</td>
    </tr>
    <tr>
        <td>UserCreate</td>
        <td>/users/create</td>
    </tr>
    <tr>
        <td>ResolutionCreate</td>
        <td>/resolutions/create</td>
    </tr>
    <tr>
        <td>RamCreate</td>
        <td>/rams/create</td>
    </tr>
    <tr>
        <td>ShowSmartphone</td>
        <td>/smartphones/:id</td>
    </tr>
</table>

## License
"Vue Phone-Mania-Laravel" is an open-sourced software licensed under the MIT license(https://opensource.org/licenses/MIT).
